/// This file simply runs all tests that dont't require user input.
/// It's not overly helpful though, since it's just an endless spam of text, but it keeps track to make sure all the tests are working.
module tests;

import std.stdio, std.file, std.path, std.algorithm, std.process;

void main()
{
    struct Failed
    {
        string test;
        string output;
    }

    string[] banned = 
    [
    ];

    auto entries = dirEntries("", SpanMode.shallow).filter!((entry) => entry.isDir).filter!((entry) => !banned.any!((b) => entry.name.baseName.canFind(b)));
    auto cwd     = getcwd();

    Failed[] failed;
    foreach(entry; entries)
    {
        chdir(entry.name);

        auto result = execute(["dub", "test"]);
        if(result.status != 0)
            failed ~= Failed(entry.name, result.output);

        writeln(result.output);
        chdir(cwd);
    }

    writeln("\n\n\n");

    foreach(failure; failed)
        writefln("'%s' failed: %s\n\n", failure.test, failure.output);
}