import std.stdio;
import std.exception : assertThrown;
import jlua.lua;
import derelict.lua.lua;

string code = 
r"
local args = {...}

if (args[1] == 'Daniel') then return 'Is Souper'
else                          return 23 end
";

void main()
{
    DerelictLua.load("../../../lib/lua53.dll");

    auto state = new LuaState();
    state.openDefaultLibs();
    state.runString!false(code, "Daniel");

    assert(state.isType!string(-1));
    assert(state.get!string(-1) == "Is Souper", state.get!string(-1));
    state.pop(1);

    state.runString!false(code, 20);

    assert(state.isType!int(-1));
    assert(state.get!int(-1) == 23);

    state.push("Soup");
    assertThrown!LuaException(state.get!bool(-1));
}
