import std.stdio;
import derelict.lua.lua;
import jlua.lua;

string code = 
r"
assert(type(daniel) == 'string')

if (daniel == 'Sexy') then
    print('I agree with this statement')
else
    print('Dan is so sexy')
end
";

void main()
{
    DerelictLua.load("../../../lib/lua53.dll");

    auto state = new LuaState();
    state.openDefaultLibs();
    state.setGlobal("daniel", "Sexy");
    state.runString(code);
}
