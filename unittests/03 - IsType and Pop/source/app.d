import std.stdio;
import std.exception : assertThrown;
import derelict.lua.lua;
import jlua.lua;

void main()
{
    DerelictLua.load("../../../lib/lua53.dll");

    auto state = new LuaState();
    state.push("Hello World");
    state.push(23);
    state.push(true);

    assert(state.isType!bool(-1));
    assert(state.isType!int(-2));
    state.pop(2);
    assert(state.isType!string(-1));

    assertThrown!LuaException(state.pop(lua_gettop(state.state) + 1));
}
