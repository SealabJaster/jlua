import std.stdio;
import std.exception : assertThrown;
import jlua.lua;
import derelict.lua.lua;

string code = 
r"
num = num * 2
";

void main()
{
    DerelictLua.load("../../../lib/lua53.dll");

    auto state = new LuaState();
    state.openDefaultLibs();
    state.setGlobal("num", 28);
    state.runString(code);

    assert(state.getGlobal!int("num") == 28 * 2);
}
