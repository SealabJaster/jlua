import std.stdio;
import std.exception : assertThrown;
import jlua.lua;
import derelict.lua.lua;

string code = 
r"
local args = {...}

for i, v in ipairs(args) do
    if (type(v) == 'boolean') then v = v and 'true' or 'false' end -- Yea, confuses me as well

    print('Arg #' .. i .. ': ' .. v)
end
";

void main()
{
    DerelictLua.load("../../../lib/lua53.dll");

    auto state = new LuaState();
    state.openDefaultLibs();
    state.runString(code, "Hello", 23, true, ["Daniel is", "Such a man"]);

    assertThrown!LuaException(state.runString("&(()&( Invalid lua code!"));
    assertThrown!LuaException(state.runString("assert(9 + 10 == 21, 'Ive failed life')"));
}
