﻿module jlua.lua;

private
{
    import derelict.lua.lua;
}

/++
 + Contains an enumeration of the different types of a Lua value.
 + 
 + See_Also:
 +  `toLuaType`
 + ++/
enum LuaType : int
{
    /// A String
    String  = LUA_TSTRING,

    /// A number
    Number  = LUA_TNUMBER,

    /// A boolean
    Boolean = LUA_TBOOLEAN,

    /// No value
    Nil     = LUA_TNIL,

    /// Unknown type (Used for error checking in templates)
    Unknown = LUA_TNONE
}

/// An alias that can be used to represent Lua's `nil`
alias Nil = typeof(null);

/++
 + Converts `T` into it's respective `LuaType`
 + ++/
template toLuaType(T)
{
    import std.traits : isNumeric, isBoolean;

    static if(is(T == string))
        alias toLuaType = LuaType.String;
    else static if(isNumeric!T)
        alias toLuaType = LuaType.Number;
    else static if(isBoolean!T)
        alias toLuaType = LuaType.Boolean;
    else static if(is(T == Nil))
        alias toLuaType = LuaType.Nil;
    else
        alias toLuaType = LuaType.Unknown;
}
///
unittest
{
    static assert(toLuaType!string == LuaType.String);
    static assert(toLuaType!int    == LuaType.Number);
    static assert(toLuaType!float  == LuaType.Number);
    static assert(toLuaType!bool   == LuaType.Boolean);
    static assert(toLuaType!Nil    == LuaType.Nil);
}

/++
 + Provides access to using Lua.
 + ++/
final class LuaState
{
    import std.exception    : enforce;
    import std.traits       : allSatisfy;

    private
    {
        char[2048]      _buffer; // LuaState will try to use this buffer to add a null-terminator to strings, and fall back to std.string.toStringz(which copies it) if it's too large.
        size_t          _index;
        lua_State*      _state;

        // Will try to use a pre-allocated buffer to make the string, and falls back to the normal toStringz if `s` is too big.
        @trusted
        const(char)* toStringz(string s)
        {
            import std.string : toStringz;

            if(s.length >= this._buffer.length - 1) // - 1, so we know there's enough space for the \0 
                return s.toStringz;

            this._buffer[0..s.length] = s;
            this._buffer[s.length]    = '\0';

            return this._buffer[0..s.length + 1].ptr;
        }

        void onError(bool errorOnStack = false)(const(char)[] message, size_t toPop = 0)
        {
            import std.format : format;
            string msg;

            static if(errorOnStack)
            {
                assert(lua_isstring(this.state, -1), "No error string is at the top of the stack");

                size_t length;
                auto   Cstring = lua_tolstring(this.state, -1, &length);
                msg            = format("'%s' : '%s'", message, Cstring[0..length]);

                lua_pop(this.state, 1);
            }
            else
                msg = message.idup;

            if(toPop != 0)
                lua_pop(this.state, toPop);

            throw new LuaException(msg);
        }

        // These are just to make sure the functions all compile
        version(unittest)
        {
            void __test()
            {
                import std.typecons : AliasSeq;
                alias types         = AliasSeq!(string, bool, int, float, Nil);
                alias getTypes      = AliasSeq!(string, bool, int, float);

                // push
                foreach(type; types)
                {
                    alias x = push!type;
                    alias y = push!(type[]);
                }

                // runString
                alias rst_ = runString!(true, types);
                alias rsf_ = runString!(false, types);

                // isType
                foreach(type; types)
                    alias x = isType!type;

                // get
                foreach(type; getTypes)
                    alias x = get!type;

                // getGlobal
                foreach(type; getTypes)
                    alias x = getGlobal!type;
            }
        }
    }

    public
    {
        /++
         + Creates a new Lua context.
         + ++/
        @trusted @nogc
        this() nothrow
        {
            this._state = luaL_newstate();
            assert(this.state !is null, "Could not create Lua state");
        }

        ~this()
        {
            if(this.state !is null)
                lua_close(this.state);
        }

        /++
         + Pushes a value onto the stack.
         + 
         + Do note that if `value` is an array, then it's elements are pushed 1-by-1, 
         + instead of having a table made and pushed.
         + 
         + Params:
         +  value = The value to push
         + ++/
        void push(T)(T value)
        if(isPushable!T)
        {
            alias type = toLuaType!T;

            static if(isLuaTypeArray!T)
                foreach(element; value)
                    this.push(element);
            else static if(type == LuaType.String)
                lua_pushlstring(this.state, value.ptr, value.length);
            else static if(type == LuaType.Boolean)
                lua_pushboolean(this.state, cast(int)value);
            else static if(type == LuaType.Number)
                lua_pushnumber(this.state, cast(lua_Number)value);
            else static if(type == LuaType.Nil)
                lua_pushnil(this.state);
            else
                static assert(0, "Don't know how to push " ~ T.stringof ~ "; This is a bug");
        }

        /++
         + Pops a certain amount of values from the stack.
         + 
         + Throws:
         +  `LuaException` if there are not enough elements to pop.
         + 
         + Params:
         +  amount = The amount of values to pop.
         + ++/
        @trusted
        void pop(size_t amount)
        {
            if(amount > lua_gettop(this.state))
                this.onError!false("Attempting to pop more elements than avaliable");

            lua_pop(this.state, amount);
        }

        /++
         + Opens all of the default libraries for use.
         + ++/
        @trusted @nogc
        void openDefaultLibs() nothrow
        {
            luaL_openlibs(this.state);
        }

        /++
         + Executes a piece of Lua code.
         + 
         + Throws:
         +  `LuaException` if some kind of error occurs.
         + 
         + Params:
         +  [cleanStack] = If `true`, then any values returned will be popped off the stack.
         +                 If `false`, then any values returned will be kept on the stack.
         +  code         = The code to execute.
         +  args         = The args to give the function.
         + ++/
        void runString(bool cleanStack = true, Args...)(string code, Args args)
        if(allSatisfy!(isPushable, Args))
        {
            auto stackBefore = lua_gettop(this.state);
            scope(exit)
                static if(cleanStack)
                    lua_settop(this.state, stackBefore);

            // Load the code
            auto result = luaL_loadstring(this.state, this.toStringz(code));
            if(result  != LUA_OK)
                this.onError!true("Error while loading Lua code");

            // Push the args, and make sure the code executes properly
            foreach(arg; args)
                this.push(arg);

            // "stacBefore + 1", the + 1 is because we need to not include the function itself
            auto amountPushed = lua_gettop(this.state) - (stackBefore + 1);
            result            = lua_pcall(this.state, amountPushed, LUA_MULTRET, 0);
            if(result != LUA_OK)
                this.onError!true("Error while executing Lua code");
        }

        /++
         + Checks to see if the value at `index` on the stack is convertible to type `[T]`.
         + 
         + Params:
         +  [T]     = The type to see if the variable is convertable to.
         +  index   = The index of the variable on the stack to check.
         + 
         + Returns:
         +  `True` if the variable at `index` is convertable to `[T]`.
         +  `LuaType.Unknown` may be returned if the index is not valid.
         + ++/
        bool isType(T)(int index)
        if(isLuaType!T)
        {
            alias type = toLuaType!T;

            static if(type == LuaType.Boolean)
                alias func = lua_isboolean;
            else static if(type == LuaType.Nil)
                alias func = lua_isnil;
            else static if(type == LuaType.Number)
                alias func = lua_isnumber;
            else static if(type == LuaType.String)
                alias func = lua_isstring;
            else
                static assert("Invalid type: " ~ T.stringof ~ "(" ~ type.stringof ~ ")");

            return (func(this.state, index) == 1);
        }

        /++
         + Determines the type of the variable at `index` on the stack.
         + 
         + Params:
         +  index = The index of the variable to get the type of.
         + 
         + Returns:
         +  The `LuaType` of the variable at `index`.
         + ++/
        @trusted @nogc
        LuaType getType(int index) nothrow
        {
            return cast(LuaType)lua_type(this.state, index);
        }

        /++
         + Gets the value of a variable in the stack.
         + 
         + Note that this function does not remove the variable from the stack.
         + 
         + $(B It is important to note that if `[T]` is a `string`, then a `const(char)[]` is returned and
         + should be copied before the variable is popped off the stack)
         + 
         + Throws:
         +  `LuaException` if the variable is not of a compatable type with `[T]`
         + 
         + Params:
         +  [T]   = The type to cast the variable to.
         +  index = The index of the variable on the stack to get the value of.
         + 
         + Returns:
         +  The value of the variable at `index`, casted to `[T]`.
         +  For strings, a `const(char)[]` is returned instead.
         + ++/
        auto get(T)(int index)
        if(isLuaType!T)
        {
            import std.format : format;
            alias type = toLuaType!T;

            if(!this.isType!T(index))
                this.onError(format("Cannot convert a variable of type '%s'(stack index of '%s') to '%s.", 
                                    this.getType(index), index, type));

            static if(type == LuaType.Boolean)
                return cast(bool)lua_toboolean(this.state, index);
            else static if(type == LuaType.Number)
                return cast(T)lua_tonumber(this.state, index);
            else static if(type == LuaType.String)
            {
                size_t length;
                auto pointer = lua_tolstring(this.state, index, &length);

                return pointer[0..length];
            }
            else
                static assert(0, format("Don't know how to get a value of: %s(%s)", T.stringof, type));
        }

        /++
         + Sets the value of the global variable `varName` to `value`.
         + 
         + Params:
         +  varName = The name of the global variable.
         +  value   = The value to assign to the variable.
         + ++/
        void setGlobal(T)(string varName, T value)
        if(isLuaType!T)
        {
            this.push(value);
            lua_setglobal(this.state, this.toStringz(varName));
        }

        /++
         + Pushes a global variable onto the stack.
         + 
         + Note that if the variable doesn't exist, then "Nil" will be pushed.
         + (It doesn't seem possible to tell between "variable's value is Nil" and "variable doesn't exist", so an exception can't be thrown)
         + 
         + Params:
         +  varName = The name of the variable to push.
         + ++/
        @trusted
        void pushGlobal(string varName)
        {
            lua_getglobal(this.state, this.toStringz(varName));
        }

        /++
         + Gets the value of a global variable.
         + 
         + Throws:
         +  See `LuaType.pushGlobal` and `LuaType.get`
         + 
         + Parameters:
         +  varName = The name of the global variable to get a copy of.
         + 
         + Returns:
         +  The value of `varName`. If `[T]` is a `string`, then a GC-allocated copy is returned.
         + ++/
        @trusted
        auto getGlobal(T)(string varName)
        if(isLuaType!T)
        {
            this.pushGlobal(varName);
            auto value = this.get!T(-1);
            this.pop(1);

            static if(toLuaType!T == LuaType.String)
                return value.idup;
            else
                return value;
        }

        /++
         + Returns the underlying lua_State*
         + ++/
        @property @safe @nogc
        lua_State* state() nothrow pure
        out(ptr)
        {
            assert(ptr !is null);
        }
        body
        {
            return this._state;
        }
    }
}

/// Thrown whenever something goes wrong with Lua.
class LuaException : Exception
{
    /**
     * Creates a new instance of Exception. The next parameter is used
     * internally and should always be $(D null) when passed by user code.
     * This constructor does not automatically throw the newly-created
     * Exception; the $(D throw) statement should be used for that purpose.
     */
    @nogc @safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super(msg, file, line, next);
    }
}

/++
 + A combination of `isLuaType` and `isLuaTypeArray`.
 + ++/
template isPushable(T)
{
    enum isPushable = (isLuaType!T || isLuaTypeArray!T);
}

/++
 + Determines if `T` is an array of a type that represnets one of Lua's basic types.
 + 
 + See_Also:
 +  `isLuaType`
 + ++/
template isLuaTypeArray(T)
{
    import std.traits : isDynamicArray;
    import std.range  : ElementEncodingType;

    enum isLuaTypeArray = (isDynamicArray!T && isLuaType!(ElementEncodingType!T));
}
///
unittest
{
    static assert(isLuaTypeArray!(string[]));
    static assert(isLuaTypeArray!(int[]));
    static assert(isLuaTypeArray!(float[]));
    static assert(isLuaTypeArray!(bool[]));

    static assert(!isLuaTypeArray!(LuaState[]));
}

/++
 + Determines if `T` can represent one of Lua's basic types (string, number, boolean).
 + 
 + See_Also:
 +  `isLuaTypeArray`
 + ++/
template isLuaType(T)
{
    enum isLuaType = (toLuaType!T != LuaType.Unknown);
}
///
unittest
{
    static assert(isLuaType!float);
    static assert(isLuaType!int);
    static assert(isLuaType!bool);
    static assert(isLuaType!string);

    static assert(!isLuaType!LuaState);
}